# urlbar -- a simple iframe browser with URL bar and onscreen keyboard

This is a simple webapp that provides an iframe browser with a URL bar that can
be edited with a toggleable onscreen keyboard. Made for [Jonte](https://jontes.page/)'s
Nest Hub.

An instance is running on my site [here](https://urlbar.spitemim.xyz/).

## todo

* let the onscreen keyboard interact with the webpage, not just the URL bar
* back/forward buttons
* code cleanup, more comments
* bypass checks that block iframes from visiting many sites -- no idea if this
  is possible to circumvent. maybe a server-side proxy is the better idea for this.

## license

This program is free software, licensed under the GNU GPLv3.
